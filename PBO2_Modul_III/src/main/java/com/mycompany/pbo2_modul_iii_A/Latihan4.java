/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_modul_iii_A;

import java.awt.TextField;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Latihan4 extends JFrame{

    public Latihan4() {
        this.setLayout(null);
        this.setSize(500, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Find");
        this.setVisible(true);
        
        JLabel label = new JLabel();
        TextField text = new TextField();
        JButton tombol =new JButton();
        
        label.setText("Keyword");
        label.setBounds(200, 20, 110, 20);
        this.add(label);
        
        
        text.setBounds(150, 50, 150, 20);
        this.add(text);
        
        tombol.setText("Find");
        tombol.setBounds(170,80, 100, 20);
        this.add(tombol);
    }
    public static void main(String[] args) {
        new Latihan4();
    }

}
