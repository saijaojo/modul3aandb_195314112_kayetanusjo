/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

/**
 *
 * @author asus
 */
public class Mahasiswa extends Penduduk{
    private String nim;

    public Mahasiswa() {
    }

    public Mahasiswa(String nim, String name, String tempat_Tanggal_Lahir) {
        super(name, tempat_Tanggal_Lahir);
        this.nim = nim;
       
    }

    public Mahasiswa(String nim) {
        this.nim = nim;
    }
     String gerNim (){
         return nim;
     }
     void setNim (String dataNim){
         this.nim =dataNim;
     }

    @Override
    public double hitungIuran() {
        double nilai = Double.parseDouble(nim);
        return nilai/10000;
    }
}
